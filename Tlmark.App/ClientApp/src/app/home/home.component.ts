import { Component } from '@angular/core';
import { UsuariosService} from '../services/usuarios.service';
import {Usuario} from '../entities/usuario';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  usuarios: any = [];
  usuario: any;

  constructor ( private _usuariosService: UsuariosService)
  {
    this.ObtenerUsuarios();
  }
   

  ObtenerUsuarios(){
    this.usuarios = this._usuariosService.ObtenerUsuarios();
    console.log(this.usuarios);
  }

  ObtenerUsuario(Id: number){
    this.usuario = this._usuariosService.ObtenerUsuario(Id);
  }

  GuardarUsuario(usuario: Usuario){
    this._usuariosService.GuardarUsuario(usuario);
  }

  BorrarUsuario(Id: number){
     var resp =  this._usuariosService.BorrarUsuario(Id);
  }
  


}
