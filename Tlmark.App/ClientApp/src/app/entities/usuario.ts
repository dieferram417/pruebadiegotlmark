export class Usuario {
    UsuarioId: number;
    nombre: string;
    apellido: string;
    dni?: number;
    usuarioRed: string;
    correo: string;
    fechaNac?: Date;
    FechaBaja?: Date;
}