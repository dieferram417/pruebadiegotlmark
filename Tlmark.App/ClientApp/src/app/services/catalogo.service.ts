// Modules
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';



@Injectable()
export class CatalogosService {

  constructor(private http: Http) { }

  /**
   * Retorna la raíz de la URL del servidor BackEnd
   */
  getApiUrl() {
    return this.http.get("./assets/config.json").toPromise().then(r => { return r.json().tlmarkApi; });
  }


}
