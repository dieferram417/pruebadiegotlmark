import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import {Usuario} from '../entities/usuario';

import { CatalogosService } from './catalogo.service';

@Injectable()
export class UsuariosService {
    constructor(private http: Http, 
                private catalogo: CatalogosService){
    }
    
    ObtenerUsuarios(): Promise<Array<Usuario>>{  
        return this.catalogo.getApiUrl().then(r => {
          return this.http.get(r + "api/usuarioApi/usuarios").toPromise().then(r => {
            return r.json() as Array<Usuario>;
          }).catch(r=>{return null;});
        });
      }

      ObtenerUsuario(param:number): Promise<Usuario>{  
        return this.catalogo.getApiUrl().then(r => {
          return this.http.post(r + "api/usuarioApi/usuario",param).toPromise().then(r => {
            return r.json() as Usuario;
          }).catch(r=>{return null;});
        });
      }

      GuardarUsuario(param:Usuario): Promise<Usuario>{  
        return this.catalogo.getApiUrl().then(r => {
          return this.http.post(r + "api/usuarioApi/guardarusuario",param).toPromise().then().catch(r=>{return null;});
        });
      }

      BorrarUsuario(param:number): Promise<Usuario>{  
        return this.catalogo.getApiUrl().then(r => {
          return this.http.post(r + "api/usuarioApi/borrarusuario",param).toPromise().then(r => {
            return r.json() as boolean;
          }).catch(r=>{return null;});
        });
      }


}