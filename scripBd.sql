USE [master]
GO

CREATE DATABASE [tlmatk]
GO

USE  [tlmatk]
GO

CREATE TABLE [dbo].[usuario](
	[UsuarioId] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NULL,
	[apellido] [nvarchar](50) NULL,
	[dni] [int] NULL,
	[usuarioRed] [nvarchar](50) NULL,
	[correo] [nvarchar](50) NULL,
	[fechaNac] [datetime] NULL,
	[fechaBaja] [datetime] NULL,
 CONSTRAINT [PK_usuario] PRIMARY KEY CLUSTERED 
(
	[UsuarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT INTO [dbo].[usuario]
           ([nombre]
           ,[apellido]
           ,[dni]
           ,[usuarioRed]
           ,[correo]
           ,[fechaNac]
           ,[fechaBaja])
     VALUES
           ('Pepito'
           ,'perez'
           ,1234
           ,'pepito.perez'
           ,'pepito.perez@mail.com'
           ,'1983-09-01'
           ,null)
GO



