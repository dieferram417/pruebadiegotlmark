﻿
Imports System.Web.Http
Imports Tlmark.Data
Imports Tlmark.Dominio

Namespace Controllers
    <RoutePrefix("api/usuarioApi")>
    Public Class UsuarioController
        Inherits ApiController

        Private servicio As IServicioUsuarios = New ServicioUsuarios()

        <Route("usuarios")>
        <HttpGet>
        Public Function GetUsuarios(
        <FromBody> ByVal body As List(Of UsuarioApiMondel)) As IHttpActionResult

            Dim usuarioList As List(Of UsuarioApiMondel) = New List(Of UsuarioApiMondel)()
            Dim usuarioItem As UsuarioApiMondel = New UsuarioApiMondel()
            For Each item As usuario In servicio.ObtenerUsuarios
                usuarioItem.UsuarioId = item.UsuarioId
                usuarioItem.nombre = item.nombre
                usuarioItem.apellido = item.apellido
                usuarioItem.dni = item.dni
                usuarioItem.usuarioRed = item.usuarioRed
                usuarioItem.correo = item.correo
                usuarioItem.fechaNac = item.fechaNac
                usuarioItem.FechaBaja = item.fechaBaja
                usuarioList.Add(usuarioItem)
            Next

            Return Ok(usuarioList.ToArray())
        End Function


        <Route("guardarusuario")>
        <HttpPost>
        Public Function SaveUsuarios(
        <FromBody> ByVal body As UsuarioApiMondel) As IHttpActionResult
            Dim user As usuario = New usuario()
            user.UsuarioId = body.UsuarioId
            user.nombre = body.nombre
            user.apellido = body.apellido
            user.dni = body.dni
            user.usuarioRed = body.usuarioRed
            user.correo = body.correo
            user.fechaNac = body.fechaNac
            user.fechaBaja = body.FechaBaja

            servicio.GuardarUsuario(user)

            Return Ok()
        End Function



        <Route("usuario")>
        <HttpPost>
        Public Function getUsuario(
        <FromBody> ByVal Id As Integer) As IHttpActionResult
            Dim user As usuario = servicio.ObtenerUsuario(Id)
            Dim userReturn As UsuarioApiMondel = New UsuarioApiMondel()
            userReturn.UsuarioId = user.UsuarioId
            userReturn.nombre = user.nombre
            userReturn.apellido = user.apellido
            userReturn.dni = user.dni
            userReturn.usuarioRed = user.usuarioRed
            userReturn.correo = user.correo
            userReturn.fechaNac = user.fechaNac
            userReturn.FechaBaja = user.fechaBaja

            Return Ok(userReturn)

        End Function

        <Route("borrarusuario")>
        <HttpPost>
        Public Function deleteUsuario(
        <FromBody> ByVal Id As Integer) As IHttpActionResult
            Dim result As Boolean = servicio.BorrarUsuario(Id)
            Return Ok(result)

        End Function
    End Class
End Namespace