﻿Imports Tlmark.Data

Public Interface IServicioUsuarios
    Function ObtenerUsuarios() As List(Of usuario)
    Sub GuardarUsuario(ByVal data As usuario)
    Function ObtenerUsuario(Id As Integer) As usuario
    Function BorrarUsuario(Id As Integer) As Boolean

End Interface
