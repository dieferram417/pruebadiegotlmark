﻿Imports Tlmark.Data
Imports Tlmark.Dominio

Public Class ServicioUsuarios
    Implements IServicioUsuarios

    Private repositorio As IRepositorioUsuario = New RepositorioUsuario()

    Public Sub GuardarUsuario(data As usuario) Implements IServicioUsuarios.GuardarUsuario
        repositorio.GuardarUsuario(data)
    End Sub

    Public Function ObtenerUsuarios() As List(Of usuario) Implements IServicioUsuarios.ObtenerUsuarios
        Return repositorio.ObtenerUsuarios
    End Function

    Public Function ObtenerUsuario(Id As Integer) As usuario Implements IServicioUsuarios.ObtenerUsuario
        Return repositorio.ObtenerUsuario(Id)
    End Function

    Public Function BorrarUsuario(Id As Integer) As Boolean Implements IServicioUsuarios.BorrarUsuario
        Return repositorio.BorrarUsuario(Id)
    End Function
End Class


