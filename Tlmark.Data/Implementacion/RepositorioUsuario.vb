﻿Imports Tlmark.Data

Public Class RepositorioUsuario
    Implements IRepositorioUsuario

    Dim context As tlmatkEntities = New tlmatkEntities()

    Public Function ObtenerUsuarios() As List(Of usuario) Implements IRepositorioUsuario.ObtenerUsuarios
        Return context.usuario.ToList()
    End Function

    Public Sub GuardarUsuario(ByVal data As usuario) Implements IRepositorioUsuario.GuardarUsuario
        context.usuario.Add(data)
        context.SaveChanges()
    End Sub

    Public Function ObtenerUsuario(ByVal Id As Integer) As usuario Implements IRepositorioUsuario.ObtenerUsuario
        Return context.usuario.Find(Id)
    End Function

    Public Function BorrarUsuario(ByVal Id As Integer) As Boolean Implements IRepositorioUsuario.BorrarUsuario
        Dim user As usuario = context.usuario.Find(Id)
        context.usuario.Remove(user)
        context.SaveChanges()
        Return True
    End Function
End Class
